import React from 'react';
import {connect}  from 'react-redux'
import ExpenseListItem from './ExpenseListItems';
import ExpenseListItems from './ExpenseListItems'
import SelectExpense from '../selectors/expense'
const Expsneslist=(props)=>(
    <div>
    


    {props.expenses.map((expense)=>{
        return <ExpenseListItem key={expense.id} {...expense} />
    })}
    </div>
)

const ConnectExpenseList=connect((state)=>{
    return {
        expenses:SelectExpense(state.expenses,state.filters)
       
    }
})(Expsneslist)
export default ConnectExpenseList